import json
import nltk
import gzip
from collections import defaultdict, Counter

nltk.download('punkt')

def charger_crawled_urls(path='index/json/crawled_urls.json'):
    ''' Fonction qui extrait les données d'un fichier json
    '''
    with open(path, 'r', encoding='utf-8') as file:
        return json.load(file)

def tokenize(text):
    ''' Fonction qui tokenise un texte
    '''
    tokens = nltk.word_tokenize(text)
    
    # On lowerise chaque token
    tokens_lower = [token.lower() for token in tokens]
    return tokens_lower

def stemme(tokens):
    ''' Fonction qui stemme les tokens (allons --> all). On utilise l'algorithme de Porter ici.
    '''
    stemmer = nltk.stem.PorterStemmer()
    return [stemmer.stem(token) for token in tokens]

def creer_fichier(filename, file, indent=None):
    ''' Fonction qui crée un fichier json à partir d'un dictionnaire python
    '''
    path = f"index/json/{filename}.json"
    with open(path, 'w') as fichier:
        json.dump(file, fichier, indent=indent)


# On charge la liste d'URLs depuis le fichier JSON
liste_articles = charger_crawled_urls()

def index_de_l_item(item: str):
    ''' Fonction qui crée les 3 index selon l'item extrait : titre, h1 ou content
    '''

    # On initialise notre index non positionnel
    non_pos_index = defaultdict(list)

    # On initialise notre index non positionnel avec stemming
    stemmer_index = defaultdict(list)

    # On initialise notre index positionnel
    pos_index = defaultdict(lambda: defaultdict(list))

    # On initialise les statistiques
    nbr_documents = len(liste_articles)  
    nbr_tokens_global = 0
    nbr_tokens_par_champ = defaultdict(int) #on utilise cette méthode qui crée des clefs selon les tokens et initialisent leur compteur à 0
    liste_tokens = [] # on utilise cette variable pour calculer les 100 tokens les plus fréquents

    for index in range(len(liste_articles)):    # Pour chaque URL crawlée,
        article = liste_articles[index]
        item_extrait = article.get(item, '')            # On extrait l'item 
    
        item_tokens = tokenize(item_extrait)          # on le tokenise 
    
    ##########################################################
    ############# Index inversé non positionnel ##############
    ##########################################################
    
        for token in set(item_tokens):          # Pour chaque token 
            non_pos_index[token].append(index)  # on crée la clef associée et on ajoute l'indice de l'article auquel il appartient
        
    ##########################################################
    ###### Index inversé non positionnel avec stemming #######
    ##########################################################
            
        item_tokens_stemmed = stemme(item_tokens) # on stemme le titre
    
        for token_stemmed in set(item_tokens_stemmed): # on ajoute tous les tokens stemmés à l'index stemmed
            stemmer_index[token_stemmed].append(index)
    
    ##########################################################
    ############### Index inversé positionnel ################
    ##########################################################
    
        for position, token in enumerate(item_tokens):  # on ajoute tous les tokens à l'index positionnel
            if index not in pos_index[token]:          # Si l'index n'existe pas encore pour ce token
                pos_index[token][index] = {"positions": [position], "count": 1}
            else:
                pos_index[token][index]["positions"].append(position)
                pos_index[token][index]["count"] += 1
    
    ##########################################################
    ############### Calcul des statistiques ##################
    ##########################################################
        
        # On met à jour les stat
        for champ in ['title', 'content', 'h1']:
            champ_tokens = tokenize(article.get(champ, ''))
            nbr_tokens_par_champ[champ] += len(champ_tokens)
            liste_tokens+=champ_tokens

            nbr_tokens_global += nbr_tokens_par_champ[champ]
            print(nbr_tokens_global)

    # On calcule le nombre moyen de tokens par documents
    moyenne_tokens_par_document = nbr_tokens_global / nbr_documents

    top100_tokens = dict(Counter(liste_tokens).most_common(100))
  
    # On enregistre les metadata pour les stocker dans le fichier metadata.json
    metadata = {'nbr_documents': nbr_documents,
                'nbr_tokens_global': nbr_tokens_global,
                'nbr_tokens_par_champ': nbr_tokens_par_champ,
                'moyenne_tokens_par_document': moyenne_tokens_par_document,
                'top100_tokens' : top100_tokens
                }
    
    creer_fichier('metadata', metadata, 2)
    
    # On enregistre l'index non positionnel dans un fichier
    creer_fichier(f"{item}index/{item}.non_pos_index", non_pos_index)
    
    # On enregistre l'index avec le stemming dans un fichier
    creer_fichier(f"{item}index/mon_stemmer.{item}.non_pos_index", stemmer_index)
    
    # On enregistre l'index positionnel dans un fichier
    creer_fichier(f"{item}index/{item}.pos_index", pos_index)

    print("C'est cuit !")

############################################################################
#########   DÉ/COMPRESSION DU FICHIER `content.pos_index.json`  #########
############################################################################

def compresser_json(input_filename, output_filename):
    ''' Fonction qui compresse un fichier en gz
    '''
    with open(input_filename, 'rb') as f_in:
        with gzip.open(output_filename + '.gz', 'wb') as f_out:
            f_out.writelines(f_in)

def decompresser_json(input_filename, output_filename):
    ''' Fonction qui décompresse un fichier gz
    '''
    with gzip.open(input_filename, 'rb') as f_in:
        with open(output_filename, 'wb') as f_out:
            f_out.writelines(f_in)

#compresser_json(input_filename = 'index/json/contentindex/content.pos_index.json',
#                output_filename = 'index/json/contentindex/content.pos_index.json')

#decompresser_json(input_filename='index/json/contentindex/content.pos_index.json.gz',
#                  output_filename='index/json/contentindex/content.pos_index.json')