# TP2 - Indexation Web : Construire un index minimal

## Présentation du sujet

Ce projet implémente plusieurs index web. À partir d'une liste d'URLs fournie au format JSON générée par un crawler, le programme extrait les titres (ou d'autres balises intéressantes), effectue la tokenization, et construit un index web selon l'algorithme présenté en cours. Plusieurs versions d'index sont programmée : 
- Un index non positionnel, stocké dans le fichier `json/titleindex/title.non_pos_index.json`
- Un index non positionnel utilisant des stemmed tokens, stocké dans le fichier `json/titleindex/mon_stemmer.title.non_pos_index.json`
- Un index positionnel stocké dans le fichier `json/titleindex/title.pos_index.json`.

Le programme génère des statistiques sur les documents, telles que le nombre total de documents, le nombre total de tokens, le nombre de tokens par champ, la moyenne des tokens par document et les 100 tokens les plus fréquents des pages crawlées, stockées dans le fichier `metadata.json`.

## Fonctionnalités

- Extraction d'items depuis les URLs (`title`, `content`, `h1`)
- Tokenization des items extraits
- Stemming des tokens
- Construction d'un index web non positionnel
- Construction d'un index web positionnel
- Calcul des statistiques sur les documents
- Écriture de l'index et des statistiques dans un fichier JSON

## Structure
``` 
index/  
│ main.py  
│ requirements.txt  
│ README.md  
│   
└───json/    
    │ crawled_urls.json   
    │ index.json  
    │ metadata.json  
    │  
    └───titleindex/  
        │ title.non_pos_index.json  
        │ title.pos_index.json  
        │ mon_stemmer.title.non_pos_index.json  
        
    └───h1index/  
        │ h1.non_pos_index.json  
        │ h1.pos_index.json  
        │ mon_stemmer.h1.non_pos_index.json  
        
    └───contentindex/  
        │ content.non_pos_index.json  
        │ content.pos_index.json  
        │ mon_stemmer.content.non_pos_index.json  
```
    
- `main.py`: Fichier principal pour exécuter le programme.
- `requirements.txt`: Fichier décrivant les dépendances du projet.
- `README.md`: Fichier expliquant le projet, son utilisation, et les fonctionnalités.
- `json/`: Dossier contenant tous les fichiers json du programme.
- `crawled_urls.json`: Fichier json contenant la liste des urls qui a été générée depuis un crawler.
- `index.json`: Exemple d'index non positionnel.  
- `metadata.json`: Fichier json contenant les statistiques calculées.
- `titleindex/`: Dossier contenant tous les indexs de la balise titre.
- `title.non_pos_index.json`: Index non positionnel généré par le programme.
- `title.pos_index.json`: Index positionnel généré par le programme.
- `mon_stemmer.title.non_pos_index.json`: Index non positionnel utilisant les tokens stemmés généré par le programme.
- `h1index/`: Dossier contenant tous les indexs de la balise h1.
- `contentindex/`: Dossier contenant tous les indexs de la balise content.


## Installation

Clonez le projet à l'aide de la commande suivante :

```
git clone https://gitlab.com/sania.mohamed1910/tp1-indexation-web.git
```

## Utilisation

1. Assurez-vous d'avoir Python installé sur votre machine.  
2. Installez les dépendances en exécutant 
```
pip install -r requirements.txt
```
3. Exécutez l'index web en lançant le fichier `main.py` via la commande 
```
python main.py
```


## Contributeur

Sania MOHAMED, 3AID

## Commentaire

Vu la taille des indexs pour la balise contenu, j'ai compressé l'index positionnel. 
- Pour le décompresser, il faut simplement décommenter les lignes 144 à 145 : "DÉCOMPRESSION DU FICHIER `content.pos_index.json`" dans le fichier `'index/index.py'`.
- Sinon exécutez le fichier `main.py` pour avoir tous les indexs pour tous les champs.
