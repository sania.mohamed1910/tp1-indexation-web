import nltk
nltk.download('punkt')


def tokenize(text):
    ''' Fonction qui tokenise un texte
    '''
    tokens = nltk.word_tokenize(text)
    # Loweriser chaque token
    tokens_lower = [token.lower() for token in tokens]
    return tokens_lower

def stemme1(tokens):
    ''' Fonction qui stemme les tokens (allons --> all)
    '''
    stemmer = nltk.stem.PorterStemmer()
    return [stemmer.stem(token) for token in tokens]

def stemme2(tokens):
    ''' Fonction qui stemme les tokens (allons --> all)
    '''
    stemmer = nltk.stem.LancasterStemmer()
    return [stemmer.stem(token) for token in tokens]

def stemme3(tokens):
    ''' Fonction qui stemme les tokens (allons --> all)
    '''
    stemmer = nltk.stem.ISRIStemmer()
    return [stemmer.stem(token) for token in tokens]

content = "Bonjour Tahar Rahim, vous êtes un des acteurs français les plus reconnus, avec plusieurs récompenses, notamment celle du meilleur acteur avec le film 'The Mauritanian', vous nous venez tout droit d'Algérie, pourquoi ce séjour ?"

content_tokens = tokenize(content)    
print(content_tokens)

content_tokens_stemmed1 = stemme1(content_tokens)
content_tokens_stemmed2 = stemme2(content_tokens)
content_tokens_stemmed3 = stemme3(content_tokens)

print("-------------------------------\nSTEMMER PORTER\n-------------------------------")
print(content_tokens_stemmed1)
print("-------------------------------\nSTEMMER LANCASTER\n-------------------------------")
print(content_tokens_stemmed2)
print("-------------------------------\nSTEMMER ARAB\n-------------------------------")
print(content_tokens_stemmed3) #test par curiosité, on voit que ce n'est vraiment pas adapté


