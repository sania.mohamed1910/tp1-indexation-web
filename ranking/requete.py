import nltk
import json
import gzip
from nltk.corpus import stopwords

nltk.download('punkt')

def charger_json(chemin):
    ''' Fonction qui importe un fichier json
        (Cas où le fichier est compressé)
    '''
    if chemin.endswith('.gz'):
        with gzip.open(chemin, 'rt', encoding='utf-8') as fichier:
            content = json.load(fichier)
    else:
        with open(chemin, 'r', encoding='utf-8') as fichier:
            content = json.load(fichier)
    return content

def exporter_json(filepath, file_to_export, indent=None):
    ''' Fonction qui exporte du contenu en fichier JSON
    '''
    with open(filepath, 'w', encoding='utf-8') as fichier:
        json.dump(file_to_export, fichier, ensure_ascii=False, indent=indent)

def ajout_id_crawled_urls():
    ''' Cette fonction ajoute un id à chaque url du fichier
    crawled_url (pour qu'il puisse remplacer documents.json
    et éviter d'avoir 2 fichiers surper loourds qui m'empêchent de push).
    '''
    documents = charger_json('index/json/crawled_urls.json')
    for id in range(len(documents)):
        url = documents[id]
        url['id'] = id
    exporter_json('index/json/crawled_urls.json',documents, 1)
#ajout_id_crawled_urls()

def tokenize(text):
    ''' Fonction qui tokenise un texte
    '''
    tokens = nltk.word_tokenize(text)
    
    # On lowerise chaque token
    tokens_lower = [token.lower() for token in tokens]
    return tokens_lower


def transform(requete_tokens):
    ''' Fonction qui stemme les tokens (allons --> all). On utilise l'algorithme de Porter ici.
    '''
    stemmer = nltk.stem.PorterStemmer()
    return [stemmer.stem(token) for token in requete_tokens] #on peut stemmer etc selon la transformation qu'on a fait sur l'index


def filtrer_documents(index, tokens, operation='ET', collection=charger_json('index/json/crawled_urls.json')):
    ''' Fonction qui filtre les documents selon les tokens de la requête
    '''
    documents_filtres = []

    try:
        if operation == 'ET': 
            liste_documents = set(index[tokens[0]].keys())

            for token in tokens: #Pour chaque token de la requête, 
                liste_documents_du_token = index[token].keys()    # on récupère la liste des urls dans lesquels ils sont présents
                liste_documents &= set(liste_documents_du_token)  # et on ne garde que les id des urls qui contiennt tou les tokens
    
        elif operation == 'OU':
            liste_documents = set()

            for token in tokens:
                try:
                    liste_documents_du_token = index[token].keys()
                except KeyError:
                    liste_documents_du_token = []
                
                liste_documents |= set(liste_documents_du_token)

        for id in liste_documents:
            documents_filtres.append(collection[int(id)])
            
    except KeyError:
        raise ValueError("Aucun article ne correspond à votre requête.")

    return documents_filtres

nltk.download('stopwords')

# Liste des stop words en français
stop_words = set(stopwords.words('french'))


def score_nbr_occurence(requete_tokenizee, documents_filtres, index):
    ''' Fonction qui calcule un score selon le nbr d'occurrence des tokens de la requête dans le document
        Plus un token apparait dans le document, plus le score augmente
        Un poids est plus important qd les tokens ne sont pas des stopwords (bonus2)
    '''
    poids_stop_words = 0.01
    poids_mots_significatifs = 2

    for document in documents_filtres: #pour chaque document filtré
        id = document['id']            #on retrouve son id pour le chercher dans l'index
        score = 0                      #on initialise son score à 0

        for token in requete_tokenizee: #on augmente le score du nombre d'occurences des tokens d la requete
            
            # BONUS : Attribution du poids en fonction de la nature du token (stop word ou mot significatif)
            poids_token = poids_stop_words if token in stop_words else poids_mots_significatifs
            
            try:
                nbr_occurence = index[token][str(id)]['count']
            except KeyError:
                nbr_occurence = 0  # Si le token n'est pas dans l'article, on considère son nombre d'occurrence à 0
            
            score += nbr_occurence * poids_token

        document['score'] = score


def score_proximite(requete_tokenizee, documents_filtres, index, poids=1):
    ''' Fonction qui calcule un score selon la proximité entre les tokens de la requête dans le document
        S'ils sont proches comme dans la requête, le score augmente
    '''
    for document in documents_filtres:
        id = document['id']
        proximite = 0

        for token_position in range(len(requete_tokenizee) - 1):  # Parcourir les positions des tokens jusqu'au token avant-dernier
            token = requete_tokenizee[token_position]
            next_token = requete_tokenizee[token_position + 1]

            try:
                positions_token = index[token][str(id)]['positions']
                positions_next_token = index[next_token][str(id)]['positions']
            except KeyError:
                positions_token = []
                positions_next_token = []

            # Vérifier si les positions des deux tokens sont proches
            for position in positions_token:
                if abs(position - 2) in positions_next_token:  # Si le prochain token est autour du premier token (à 2 tokens prés)
                    proximite += poids
        document['proximite']= proximite

import math

def bm25_score(n, f, N, dl, avdl):
    ''' Fonction qui calcule le score bm25 terme à terme
    '''
    k1 = 1.2 #limite à quel point un seul token dans la requête peut affecter le score
    b = 0.75
    K = k1 * ((1 - b) + b * (float(dl) / float(avdl)))
    idf = math.log((N - n + 0.5) / (n + 0.5) + 1)
    return idf * ((f * (k1 + 1)) / (f + K))


def ranking_bm25(requete_tokenizee, documents_filtres, index, champ_de_index, collection=charger_json('index/json/crawled_urls.json')):
    ''' Fonction qui calcule le rank bm25
    '''
    N = len(index)  # Nombre total de documents
    avdl = sum(len(document[champ_de_index]) for document in collection) / N  # Longueur moyenne des documents

    for document in documents_filtres:
        id = document['id']
        rank_bm25 = 0

        for token in requete_tokenizee:
            try:
                n = len(index[token])  # Nombre de documents contenant le token
                f = index[token].get(str(id), {'count': 0})['count']  # Fréquence du token dans le document
                dl = len(document[champ_de_index])  # Longueur du document
    
                rank_bm25 += bm25_score(n, f, N, dl, avdl)  # Calcul du score BM25
            except KeyError:
                rank_bm25 += 0

        document['rank_bm25'] = rank_bm25


def ranking(requete_tokenizee, documents_filtres, index, champ_de_index):
    ''' Fonction qui ordonne les documents filtrés selon
        - le nombre d'occurence des tokens dans le document
        - le type de mots : stop words ou non
        - la proximité entre les tokens de la requête
        - le ranking bm25
    '''
    score_nbr_occurence(requete_tokenizee, documents_filtres, index)
    score_proximite(requete_tokenizee, documents_filtres, index)
    ranking_bm25(requete_tokenizee, documents_filtres, index, champ_de_index)

    for document in documents_filtres:
        score = document['score']
        proximite = document['proximite']
        rank_bm25 = document['rank_bm25']
        rank = score + proximite + 2*rank_bm25

        document['rank'] = rank 

    documents_ordonnes = sorted(documents_filtres, key=lambda x: x['rank'], reverse=True)

    return documents_ordonnes

def recherche(requete, operation, chemin_index, champ_de_index):
    # On charge l'index
    index = charger_json(chemin_index)

    # On tokenise et transforme la requête utilisateur
    tokens_requete = tokenize(requete)
    #tokens_requete = transform(tokens_requete)

    # On filtre les documents
    documents_filtres = filtrer_documents(index, tokens_requete, operation)

    # On ordonne les pages filtrées
    documents_filtres = ranking(tokens_requete, documents_filtres, index, champ_de_index)


    # On affiche les résultats
    resultats = {
        "requete": requete,
        "total_documents": len(index),
        "documents_filtres": len(documents_filtres),
        "resultats": [{"titre": doc['title'], "url": doc['url'], "rank":doc['rank']} for doc in documents_filtres]
    }

    # On sauvegarde les résultats dans results.json
    exporter_json('ranking/results.json', resultats, 2)

