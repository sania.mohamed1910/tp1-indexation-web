# TP3 - Indexation Web : Expansion de requête et ranking

## Présentation du sujet

Ce projet implémente un système de recherche d'information à partir d'un index créé lors du TP2. L'objectif est de permettre à l'utilisateur de soumettre une requête, de filtrer les documents pertinents et de les classer selon un score linéaire prenant en compte diverses caractéristiques : l'importance des tokens dans les documents, leurs positions et leur score bm25.

## Fonctionnalités
- Lecture d'une requête de l'utilisateur.
- Tokenisation et transformation de la requête pour filtrer les documents.
- Création d'une fonction de ranking linéaire pour ordonner les documents filtrés.
- Génération d'une liste des documents correspondants (titre + url) et nombre de documents filtrés stockés dans un fichier JSON (`results.json`). 

## Structure
``` 
ranking/  
│ main.py  
│ requete.py  
│ requirements.txt  
│ README.md  
│ results.json  

        
```
    
- `main.py`: Fichier principal pour exécuter le programme.
- `requete.py`: Fichier contenant les fonctions utilisées dans le `main.py`.
- `requirements.txt`: Fichier décrivant les dépendances du projet.
- `README.md`: Fichier expliquant le projet, son utilisation, et les fonctionnalités.
- `results.json`: Fichier json contenant les résultats de la requête

## Installation

Clonez le projet à l'aide de la commande suivante :

```
git clone https://gitlab.com/sania.mohamed1910/tp1-indexation-web.git
```

## Utilisation

1. Assurez-vous d'avoir Python installé sur votre machine.  
2. Installez les dépendances en exécutant 
```
pip install -r requirements.txt
```
3. Exécutez l'index web en lançant le fichier `main.py` via la commande 
```
python main.py
```


## Contributeur

Sania MOHAMED, 3AID

## Commentaire
