from requete import recherche
import os.path

if __name__ == "__main__":
    requete = input("\nQue souhaitez-vous chercher ? ")

    operation = input("\nSouhaitez-vous appliquer un filtre 'ET' ou 'OU' ? ")
    
    while not(operation.upper() in ['ET', 'OU']):
        operation = input("\nVeuillez choisir entre 'ET' et 'OU'. ")

    champ_de_index = input("\nSouhaitez-vous faire une recherche par titre ou contenu ? ")
    
    while not(champ_de_index.lower() in ['titre', 'contenu']):
        champ_de_index = input("\nVeuillez choisir entre 'titre' et 'contenu'. ")

    if champ_de_index.lower()=='titre':
        champ_de_index = "title"
        index = "index/json/titleindex/title.pos_index.json"
    
    elif champ_de_index.lower()=='contenu': #SI on veuut faire une recherche par contenu, il faut avoir le fichier content.pos_index.json de disponible. Or, pour des raisons d'espace, nous l'avons compressé. On décompresse alors le fichier gz (qi sera toujours là)
        champ_de_index = "content"
        if os.path.exists("index/json/contentindex/content.pos_index.json.gz"):
            index = "index/json/contentindex/content.pos_index.json.gz"
        else:
            index = "index/json/contentindex/content.pos_index.json"

    
    print("\nRecherche en cours ...")
    recherche(requete, operation.upper(), index, champ_de_index)
    print("\nLe fichier results.json est cuit.\n")
