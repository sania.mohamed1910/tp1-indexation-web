## TP d'indexation web

#### Auteur : Sania MOHAMED

Voici le dépôt Git de mes 3 tps d'indexation web.
- Le TP 1 - Création d'un crawler est localisé dans le fichier `crawler`. On crée un crawler web qui récupère au plus 50 urls visitées à partir du site https://ensai.fr/.
- Le TP 2 : Création d'un index web est localisé dans le fichier `index`. On crée plusieurs indexs inversés des titres des documents contenus dans le fichier `crawled_urls.json`. Il y a un index non positionnel, un index non positionnel basé sur les tokens stemmés et un index positionnel. On peut créer des indexs à partir des titres ou du contenu d'une page.
- Le TP 3 : Création d'un rechercheur dans un index, localisé dans le fichier `ranking`. À partir d'une requête utilisateur, on renvoie un fichier json qui regroupe tous les articles correspondant à la requête.

