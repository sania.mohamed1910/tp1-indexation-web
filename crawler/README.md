# TP1 - Indexation Web : Construire un crawler minimal

## Présentation du sujet

Ce projet implémente un web crawler simple qui explore les pages web à partir d'une URL initiale (https://ensai.fr/), respecte les règles du `robots.txt`, utilise le fichier `sitemap.xml` s'il est disponible, et enregistre les URLs trouvées dans un fichier texte dans un premier temps, puis, dans un second temps, dans une base de données relationnelle du type Sqlite3.

## Fonctionnalités

- Télécharge une page web, analyse le contenu HTML à la recherche de liens.
- Respecte les règles du `robots.txt` pour déterminer si une page est crawlable.
- Utilise le fichier `sitemap.xml` pour découvrir des liens, réduisant ainsi le nombre de requêtes.
- S'arrête après avoir exploré 5 liens par page ou atteint un maximum de 50 URLs.
- Enregistre les URLs trouvées dans le fichier `crawler/crawled_webpages_sitemap.txt`.
- Stocke les pages web trouvées et leur âge dans une base de données relationnelle.
- Respecte la politesse en attendant 3 secondes entre chaque requête.

## Structure

crawler/  
│ README.md 
│ main.py  
│ crawler.py
│ crawled_webpages.txt  
│ crawled_webpages_sitemap.txt  
│ requirements.txt  
│   
└───database/  
    │ crawled_pages.db  
  


- `main.py`: Fichier principal pour exécuter le programme.
- `crawler.py`: Contient le code du web crawler.
- `crawled_webpages.txt`: Fichier txt contenant les urls des pages web visitées par la fonction `crawler`, qui n'utilise pas les sitemaps.
- `crawled_webpages_sitemap.txt`: Fichier txt contenant les urls des pages web visitées par le crawler `crawler_sitemap` (utilisant les sitemaps). 
- `requirements.txt`: Fichier décrivant les dépendances du projet.
- `README.md`: Fichier expliquant le projet, son utilisation, et les fonctionnalités.
- `database/`: Dossier contenant le schéma de la base de données.
- `crawled_pages.db`: Base de données relationnelle contenant les urls et leur âge

## Installation

Clonez le projet à l'aide de la commande suivante :

```
git clone https://gitlab.com/sania.mohamed1910/tp1-indexation-web.git
```

## Utilisation

1. Assurez-vous d'avoir Python installé sur votre machine.
2. Installez les dépendances en exécutant 
```
pip install -r requirements.txt
```
3. Exécutez le crawler en lançant le fichier `main.py` via la commande 
```
python main.py
```
4. Pour accéder aux données stockées dans la base de données et utiliser des requêtes SQL (comme `SELECT * FROM crawled_pages;`), on exécute la commande
```
sqlite3 crawler/database/crawled_pages.db
```


## Contributeur

Sania MOHAMED, 3AID
