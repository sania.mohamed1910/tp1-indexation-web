import requests
from bs4 import BeautifulSoup
import time
import os
from urllib.parse import urljoin, urlparse
from robotexclusionrulesparser import RobotExclusionRulesParser
import sqlite3
from datetime import datetime


def save_urls_to_file(urls, file_path):
    ''' Fonction qui écrit les urls dans un fichier
    '''
    with open(file_path, 'w') as file:
        for url in urls:
            file.write(url + '\n')

def crawlability_d_une_page(url_page):
    ''' Fonction qui renvoie un booléen lorsque la page web est crawlable.
        On cherche l'information dans le robots.txt
    '''
    try:
        # Parse le domaine du lien
        domain = urlparse(url_page).netloc

        # Télécharge robots.txt
        robots_url = f'https://{domain}/robots.txt'
        robot_parser = RobotExclusionRulesParser()
        robot_parser.fetch(robots_url)

        # Vérifie si le crawl est autorisé
        return robot_parser.is_allowed("*", url_page)
    except Exception as e:
        print(f"Erreur : Site non crawlable : {url_page}: {e}")
        return False

def initialize_database():
    ''' Fonction qui initialise une base de données
    '''
    connection = sqlite3.connect('crawler/database/crawled_pages.db') #on crée une connexion à la base de données SQLite que l'on crée
    cursor = connection.cursor() #on crée un curseur qui va nous permettre de parcourir et mdoifier la db

    # On crée une table pour stocker les pages web avec 3 colonnes : un identifiant, l'url et l'âge
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS crawled_pages (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            url TEXT,
            age DATETIME
        )
    ''')

    connection.commit() # on valide les modif réalisées
    connection.close()  # on ferme le curseur

def save_page_to_database(url, age):
    ''' Fonction qui enregistre une page dans la base de données
    '''
    connection = sqlite3.connect('crawler/database/crawled_pages.db') #on initialise une connexion
    cursor = connection.cursor()

    # On ajoute la page dans la base de données
    cursor.execute(''' INSERT INTO crawled_pages (url, age)
                       VALUES (?, ?)
                   ''', (url, age)
                   )

    connection.commit()
    connection.close()

def get_page_age(url):
    ''' Fonction qui récupère l'âge stocké dans la bdd de la page pour le rafraîchir
    '''
    connection = sqlite3.connect('crawler/database/crawled_pages.db')
    cursor = connection.cursor()

    # On récupè§re l'âge de la dernière visite
    cursor.execute('''
        SELECT age FROM crawled_pages
        WHERE url = ?
        ORDER BY id DESC
        LIMIT 1
    ''', (url,))

    result = cursor.fetchone()
    connection.close()

    return datetime.strptime(result[0], '%Y-%m-%d %H:%M:%S') if result else None

def crawler(url_initiale, max_links=50):
    ''' Crawler web qui parcourt une page initiale à la recherche d'au plus 5 liens 
        puis parcourt ces 5 liens à la recherche d'au plus 5 autres liens etc.
        Jusqu'à avoir visité au plus 50 pages
        La recherche de liens se fait par fouille dans la page html.
        Les urls visitées sont stockées dans une base de données avec leur âge.
    '''
    initialize_database()     # On initialise la base de données

    visited_urls = []          # On initialise la liste de toutes les pages visitées
    to_visit = [url_initiale]  # et la liste des pages à visiter

    while to_visit and len(visited_urls) < max_links:  # Tant qu'on n'a pas visité 50 liens et qu'on a des pages à visiter
        print(len(visited_urls))
        current_url = to_visit[0]

        # Récupérer l'âge de la page
        age = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        last_visit_age = get_page_age(current_url)

        # Comparer l'âge actuel avec l'âge de la dernière visite
        if last_visit_age is not None and (datetime.now() - last_visit_age).seconds < 5:
            # Attendre jusqu'à ce que 5 secondes se soient écoulées depuis la dernière visite
            time.sleep(5 - (datetime.now() - last_visit_age).seconds)

        # Télécharger la page
        response = requests.get(current_url)

        # Analyser la page pour trouver des liens
        soup = BeautifulSoup(response.text, 'html.parser')
        links = []  # liste des liens de la page

        for a in soup.find_all('a', href=True):  # pour chaque lien trouvé sur la page
            link = a.get('href')
            if crawlability_d_une_page(link) and (link not in visited_urls + links + to_visit):
                links.append(link)

            if len(links) > 5:  # On veut au plus 5 liens par page
                break 

        to_visit += links  # On ajoute les nouveaux liens à la liste des pages à visiter        

        to_visit.remove(current_url)   #on supprime l'url actuelle des pages à visiter

        visited_urls.append(current_url) #on l'ajoute dans la liste des pages visitées

        save_page_to_database(current_url, age)  # On enregistre la page dans la base de données avec son âge
        
    #print(to_visit)
    #print(visited_urls)

    # Chemin du fichier complet
    file_path = os.path.join('crawler', 'crawled_webpages.txt')

    # Écrire les URLs trouvées dans le fichier
    save_urls_to_file(visited_urls, file_path)

    # Attendre 3 secondes avant de télécharger la prochaine page
    time.sleep(3)

def find_sitemaps_in_robots_txt(url):
    ''' Fonction qui trouve tous les sitemaps à partir du fichier robots.txt '''
    try:
        # Obtention du fichier robots.txt
        robots_url = urljoin(url, '/robots.txt')
        response = requests.get(robots_url)
        response.raise_for_status()  # Vérifie si la requête a réussi
        robots_content = response.text

        # Recherche de la directive Sitemap dans le fichier robots.txt
        sitemap_start = robots_content.find('Sitemap:')
        sitemaps = []

        while sitemap_start != -1:
            sitemap_end = robots_content.find('\n', sitemap_start)
            sitemap_url = robots_content[sitemap_start + len('Sitemap:'):sitemap_end].strip()
            sitemaps.append(urljoin(url, sitemap_url))

            # Chercher le prochain Sitemap
            sitemap_start = robots_content.find('Sitemap:', sitemap_end)

        return sitemaps
    except requests.exceptions.RequestException as e:
        print(f"Erreur lors de la recherche des sitemaps dans le fichier robots.txt : {e}")
        return []

### Exemple d'utilisation
#url_du_site = "https://ensai.fr/"
#sitemaps = find_sitemaps_in_robots_txt(url_du_site)
#print(sitemaps)

def crawler_sitemap(url_initiale, max_links=50):
    ''' Crawler web qui parcourt une page initiale à la recherche d'au plus 5 liens 
        puis parcourt ces 5 liens à la recherche d'au plus 5 autres liens etc.
        Jusqu'à avoir visité au plus 50 pages
        La recherche de liens se fait par lecture de sitemaps lorsqu'ils existent 
        pour limiter les requêtes inutiles.
        Les urls visitées sont stockées dans une base de données avec leur âge.
    '''
    initialize_database()     # On initialise la base de données

    visited_urls = []  # On initialise la liste de toutes les pages visitées
    to_visit = [url_initiale]  # et la liste des pages à visiter

    while to_visit and len(visited_urls) < max_links:
        current_url = to_visit.pop(0) #on sélectionne l'url à vister et on la supprime des pages à visiter

        ## Récupérer l'âge de la page
        age = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        last_visit_age = get_page_age(current_url)

        # Comparer l'âge actuel avec l'âge de la dernière visite
        if last_visit_age is not None and (datetime.now() - last_visit_age).seconds < 5:
            # Attendre jusqu'à ce que 5 secondes se soient écoulées depuis la dernière visite
            time.sleep(5 - (datetime.now() - last_visit_age).seconds)
        
        # Télécharger le sitemap.xml
        sitemaps = find_sitemaps_in_robots_txt(current_url)

        links = []

        if len(sitemaps)==0: # on analyse le sitemap.xml pour récupérer les URLs s'il n'est pas vide
            for sitemap in sitemaps:
                try:
                    sitemap_response = requests.get(sitemap)
                    sitemap_content = sitemap_response.text
                    sitemap_tree = BeautifulSoup(sitemap_content, 'lxml')
                except Exception as e:
                    print(f"Erreur lors du téléchargement du sitemap : {sitemap}: {e}")
                    continue
    
                for url_element in sitemap_tree.find_all('url'):
                    loc_element = url_element.find('loc')
                    if loc_element is not None:
                        link = loc_element.text
                        if link not in visited_urls + links + to_visit and crawlability_d_une_page(link):
                            links.append(link)
    
                        if len(links) >= 5:
                            break
        
        else: #on ne trouve pas de sitemap
            response = requests.get(current_url) # On télécharge la page pour recherche des urls
    
            # Analyser la page pour trouver des liens
            soup = BeautifulSoup(response.text, 'html.parser')
            links = []  # liste des liens de la page
    
            for a in soup.find_all('a', href=True):  # pour chaque lien trouvé sur la page
                link = a.get('href')
                if crawlability_d_une_page(link) and (link not in visited_urls + links + to_visit):
                    links.append(link)
    
                if len(links) > 5:  # On veut au plus 5 liens par page
                    break
            
        print(len(visited_urls))
        visited_urls.append(current_url) #on ajoute l'url actuelle dans la liste des pages visitées
        save_page_to_database(current_url, age)  # On enregistre la page dans la base de données avec son âge

        to_visit += links  # On ajoute les nouveaux liens à la liste des pages à visiter        

    # Enregistrement des URLs dans un fichier
    save_urls_to_file(visited_urls, 'crawler/crawled_webpages_sitemaps.txt')

    # Attente de 3 secondes avant de télécharger la prochaine page
    time.sleep(3)
